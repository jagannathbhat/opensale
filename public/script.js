(function () {
    var currentSec = 0, secLimit = document.getElementsByTagName('section').length, ts;

    animEnd = () => {
        document.querySelectorAll('#sec' + currentSec + ' .toAnimate').forEach((a) => {
            a.classList.remove('animating');
        });
    }

    animStart = () => {
        document.querySelectorAll('#sec' + currentSec + ' .toAnimate').forEach((a) => {
            a.classList.add('animating');
        });
    }

    secNext = () => {
        if (currentSec < secLimit - 1) {
            animEnd();
            document.getElementById('sec' + currentSec).classList.add('-hide');
            currentSec++;
            document.getElementsByTagName('body')[0].scrollTo(0, document.getElementById('sec' + currentSec).offsetTop);
            document.getElementById('sec' + currentSec).classList.remove('-hide');
            animStart();
        }
    }

    secPrev = () => {
        if (currentSec > 0) {
            animEnd();
            document.getElementById('sec' + currentSec).classList.add('-hide');
            currentSec--;
            document.getElementsByTagName('body')[0].scrollTo(0, document.getElementById('sec' + currentSec).offsetTop);
            document.getElementById('sec' + currentSec).classList.remove('-hide');
            animStart();
        }
    }

    window.addEventListener('load', () => {
        for (i = 1; i < secLimit; i++) {
            document.getElementById('sec' + i).classList.add('-hide');
        }
    });

    window.addEventListener('keyup', (e) => {
        switch (e.keyCode) {
            case 38: secPrev(); break;
            case 40: secNext(); break;
        }
    });

    window.addEventListener('touchstart', (e) => {
        ts = e.touches[0].clientY;
    });

    window.addEventListener('touchend', (e) => {
        console.log(e);
        var te = e.changedTouches[0].clientY;
        if (ts > te + 5)
            secNext();
        else if (ts < te - 5)
            secPrev();
    });

    window.addEventListener('wheel', (e) => {
        if (e.deltaY < 0)
            secPrev();
        else
            secNext();
    });
})();